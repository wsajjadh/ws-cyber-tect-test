<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    /*  public function __construct()
    {
        if (Auth::user()) {
            echo 'ok';
        } else {
            print ' no';
        }
    } */

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('user::auth.login');
    }

    public function showRegister()
    {
        return view('user::auth.register');
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users',
        ]);

        User::firstOrCreate([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return redirect('/user/login');
    }

    public function login(Request $request)

    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/user');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        echo 'padu';
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        echo 'padu';
    }
}
