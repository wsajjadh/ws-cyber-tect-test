@extends('user::layouts.master')


@section('content')
<div class="content-wrapper">

    @include('user::layouts.header')

    <div class="container">

        <div class="render-wrap"></div>
        <div id="form-render-wrap">
        </div>
        <div>
            <div id="markup"></div>
        </div>

        <button type="button" id="showForm">Show form</button>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Form Name</th>
                    <th scope="col">Form Content</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($forms as $form)
                <tr>
                    <td>{{$form->form_name}}</td>
                    <td>{{$form->form_content}}</td>
                    <td>
                        <button class="btn btn-primary" onclick="btn_click({{$form->form_content}})">Show Form</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- I don't time to make it perfect so, I keep this hidden button bcoz it's not working without it -->
    <button type="button" id="saveData" type="button" hidden>Submit</button>
    <button type="button" id="showForm" hidden>Show form</button>

</div>

<script>
    function btn_click(form) {
        console.log(`form `, form);
        /* 
         * form data should be an array to render
         */
        let formData = [];
        formData = form;
        // un used
        const escapeEl = document.createElement("textarea");
        const code = document.getElementById("markup");
        // uselessnow
        const formDatas =
            '[{"type":"number","required":false,"label":"Number","className":"form-control","name":"number-1620488317473","access":false},{"type":"radio-group","required":false,"label":"Radio Group","inline":false,"name":"radio-group-1620488317793","access":false,"other":false,"values":[{"label":"Option 1","value":"option-1","selected":false},{"label":"Option 2","value":"option-2","selected":false},{"label":"Option 3","value":"option-3","selected":false}]},{"type":"select","required":false,"label":"Select","className":"form-control","name":"select-1620488317971","access":false,"multiple":false,"values":[{"label":"Option 1","value":"option-1","selected":true},{"label":"Option 2","value":"option-2","selected":false},{"label":"Option 3","value":"option-3","selected":false}]},{"type":"select","required":false,"label":"Select","className":"form-control","name":"select-1620488318120","access":false,"multiple":false,"values":[{"label":"Option 1","value":"option-1","selected":true},{"label":"Option 2","value":"option-2","selected":false},{"label":"Option 3","value":"option-3","selected":false}]}]';

        const wrap = $(".render-wrap");
        const formRender = wrap.formRender();
        wrap.formRender({
            formData
        });
    }
</script>

@endsection