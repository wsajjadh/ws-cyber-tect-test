@extends('user::layouts.master')


@section('content')
<div class="content-wrapper">

    @include('user::layouts.header')

    <div class="container">

    <div class="render-wrap"></div>
        <div id="form-render-wrap">
        </div>
        <div>
            <div id="markup"></div>
        </div>

        <button type="button" id="showForm">Show form</button>


        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Form Name</th>
                    <th scope="col">Form Content</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($forms as $form)
                <tr>
                    <td>{{$form->form_name}}</td>
                    <td>{{$form->form_content}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>

@endsection