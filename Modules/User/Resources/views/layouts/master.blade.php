<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>User</title>

    {{-- Laravel Mix - CSS File --}}
    <link rel="stylesheet" href="{{ mix('css/user.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}"> -->
    <!-- <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}"> -->

    <!-- Other -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.11/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.11/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" />


</head>

<body class="skin-blue layout-top-nav">
    <div class="wrapper">
        @yield('content')
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>
    <script src="https://formbuilder.online/assets/js/form-render.min.js"></script>
    <!-- <script src="js/adminlte.js"></script> -->

    {{-- Laravel Mix - JS File --}}
    <script src="{{ mix('js/user.js') }}"></script>
    <script>
        jQuery($ => {
            $('.build-wrap').formBuilder()
        })
    </script>
    <script>
        $(document).ready(function() {
            /*  console.log(`loaded`);

             var fbEditor = document.getElementById("build-wrap");
             var formBuilder = $(fbEditor).formBuilder();

             document.getElementById("saveData").addEventListener("click", () => {
                 console.log('wtf ? ', formBuilder);
                 console.log("external save clicked");
                 const result = formBuilder.actions.save();
                 console.log("result:", result);
             }); */

            /*  document.getElementById("getJS").addEventListener("click", function() {
                 alert("check console");
                 console.log(formBuilder.actions.getData());
             }); */
        });
    </script>
</body>

</html>