@extends('user::layouts.master')


@section('content')
<div class="content-wrapper">

    @include('user::layouts.header')

    <div class="container">

        <h1>Create new Form</h1>
        <div class="form-group">
            <label for="exampleInputEmail1">Form Name</label>
            <input name="formName" id="formName" type="text" class="form-control" placeholder="Form Name">
        </div>

        <div class="build-wrap"></div>

        <button type="button" id="saveData" type="button">Submit</button>
        <button id="cancel" type="button">Cancel</button>
        <!-- I don't time to make it perfect so, I keep this hidden button bcoz it's not working without it -->
        <button type="button" id="showForm" hidden>Show form</button>
    </div>
</div>

@endsection