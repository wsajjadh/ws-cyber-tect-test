@extends('user::layouts.master')


@section('content')
<div class="content-wrapper">

    <h1>Create an account.</h1>

    <form method="POST" action="register">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input name="firstName" type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input name="lastName" type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-default">Create</button>
    </form>

    @endsection