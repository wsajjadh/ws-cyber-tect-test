<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->group(function () {
    /* 
        *   Middleware group is not working, I don't have time to fix
        *   So, manually !!
    */
    Route::get('/', 'UserController@index')->middleware('isLogin');  
    Route::get('/create-form', 'UserController@create')->middleware('isLogin');
    Route::post('/create-form', 'UserController@store')->middleware('isLogin');
    Route::post('/create', 'UserController@store')->middleware('isLogin');
    Route::get('/my-forms', 'UserController@show')->middleware('isLogin');
    Route::get('/form/{id}', 'UserController@showForm')->middleware('isLogin');
    
    /* Auth routes */
    Route::get('/login', 'AuthController@index')->name('login.index');
    Route::post('/login', 'AuthController@login');
    Route::get('/register', 'AuthController@showRegister');
    Route::post('/register', 'AuthController@register');
});
