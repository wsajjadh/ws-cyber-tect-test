<?php

namespace Modules\User\Entities;

use Modules\User\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Form extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'form_name',
        'form_content'
    ];

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\FormFactory::new();
    }

    public function user()
    {
       return $this->belongsTo(User::class);
    }
}
